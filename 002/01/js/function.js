window.onload = function() {

    (function() {
        var VALUE = "PARAGRAPH";
        var p = document.getElementsByTagName('p');

        for (var i = 0, max = p.length; i < max; i++) {
            p[i].innerHTML = VALUE;
        };
    }());
};