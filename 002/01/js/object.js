window.onload = function () {

    var Object = {};

    Object.VALUE = 'PARAGRAPH';

    Object.init = function() {
        var p = document.getElementsByTagName('p');

        for (var i = 0, max = p.length; i < max; i++) {
            p[i].innerHTML = this.VALUE;
        }

    }.bind(Object)();
};