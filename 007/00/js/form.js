whenReady(function() {

    function Form(form) {

        var inputs = form.getElementsByTagName('input');
        if (inputs)
            this.bind(inputs);
    }

    Form.prototype.bind = function (inputs) {
        for (var  i = 0, max = inputs.length; i < max; i++) {
            var input = inputs[i],
                required = input.getAttribute('data-required'),
                type = input.getAttribute('data-type') || "text";

            if (!required) continue;

            if (Form.events[type]) {
                var f = Form.events[type];
                input.addEventListener('change', f.bind(this), false);
            }
        }
    };

    Form.prototype.handlerText = function (e) {
        e = e || window.event;
        
        var elem = e.target,
            val = elem.value.trim();

        if (!val) {
            this.showMsg(elem);
        }
        else {
            this.removeMsg(elem);
        }
    };

    Form.prototype.handlerEmail = function (e) {
        var elem = e.target,
            val = elem.value.trim();

        var res = val.search(/\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/i);
        if (res == -1)
            this.showMsg(elem);
        else
            this.removeMsg(elem);
    };

    Form.prototype.showMsg = function (elem) {
        var is = elem.parentNode.getElementsByClassName('error')[0];
        var msg = elem.getAttribute('date-error-msg');

        elem.style.backgroundColor = 'red';

        if (!msg) return;

        if (is) {
            is.innerHTML = msg;
        }
        else {
            var msgElem = document.createElement('span');
            msgElem.appendChild(document.createTextNode(msg));
            msgElem.className = 'error';
            elem.parentNode.appendChild(msgElem);
        }
    };

    Form.prototype.removeMsg = function (elem) {
        var msg = elem.parentNode.getElementsByClassName('error')[0];
        if(msg) {
            elem.parentNode.removeChild(msg);
        }
        elem.style.backgroundColor = 'green';
    };


    Form.events = {
        "text": Form.prototype.handlerText,
        "email": Form.prototype.handlerEmail,
        "password": Form.prototype.handlerText,
        "double-password": Form.prototype.handlerPass,
    };

    var form = document.getElementsByTagName('form')[0];
    var register = new Form(form);

});