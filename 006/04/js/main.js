whenReady(function () {

    function Keymap(bindings) {
        this.map = {};
        if (bindings)
            for (var name in bindings) {
                this.bind(name, bindings[name]);
            }
    }

    Keymap.prototype.bind = function(key, func) {
        this.map[Keymap.normalize(key)] = func;
    };

    Keymap.prototype.install = function (element) {
        var keymap = this;

        function handler() {
            return keymap.dispatch(event, element);
        };

        if (element.addEventListener) {
            element.addEventListener('keydown', handler, false);
        }
        else if (element.attachEvent) {
            element.attachEvent('onkeydown', handler, false);
        }
    };

    Keymap.prototype.dispatch = function (event, element) {
        var modifairs = "";
        var keyname = null;

        if (event.altKey) modifairs += "alt_";
        if (event.ctrlKey) modifairs += "ctrl_";
        if (event.shiftKey) modifairs += "shift_";

        if (event.key) keyname = event.key;

        if (!keyname) return;

        var keyid = modifairs + keyname.toLowerCase();
        var handler = this.map[keyid];

        if (handler) {
            handler.call(element, event);
            event.preventDefault();
        }
    };

    Keymap.normalize = function (keyid) {
        keyid = keyid.toLowerCase();
        var words = keyid.split(/\s+|[\-+_]]/);
        var keyname = words.pop();
        keyname = Keymap.aliases[keyname] || keyname;
        words.sort();
        words.push(keyname);
        return words.join('_');
    };

    Keymap.aliases = {
        "ctrl": "control"
    };

    // использование
    var ctrlS = new Keymap({
        "ctrl S": function() {
            alert("Сохранено");
        },
        "ctrl A": function() {
            alert("Выбрано все");
        },
        "ctrl shift S": function() {
            alert("Сохранено все");
        }
    });

    ctrlS.install(document.body);

});