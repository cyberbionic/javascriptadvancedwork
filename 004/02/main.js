window.addEventListener('load', function() {
    
    (function() {
        
        var Auth = {
            init: function() {
                this.login = document.getElementById('login');
                this.pass = document.getElementById('pass');
                this.btn = document.getElementById('auth');
                this.error = document.getElementById('error');
                this.listeners();
            },
            listeners: function () {
                this.btn.addEventListener('click', this.authFunc.bind(this));
            },
            authFunc: function (e) {
                var login = this.login.value.trim();
                var pass = this.pass.value.trim();

                if (login == '' || pass == '') {
                    this.login.style.backgroundColor = 'red';
                    this.pass.style.backgroundColor = 'red';
                    this.showMessage('Заполните поля логин/пароль');
                    return;
                }
                if (login != 'admin' || pass != '123') {
                    this.login.style.backgroundColor = 'red';
                    this.pass.style.backgroundColor =  'red';
                    this.showMessage('Пара логин/пароль не верны');
                    return;
                } else {
                    this.login.style.backgroundColor = '#f4f4f4';
                    this.pass.style.backgroundColor = '#f4f4f4';
                    this.showMessage('Вы авторизированы');
                    return;
                }
            },
            showMessage: function (text) {
                var status = document.getElementById('status');

                if (!status) {
                    var message = document.createElement('div');
                    message.id = 'status';
                    message.className = 'auth__message';
                    message.appendChild(document.createTextNode(text));
                    document.getElementsByClassName('auth')[0].appendChild(message);
                } else {
                    status.innerHTML = text;
                }
            }
        };
        Auth.init();
        
    }());
    
}, false);